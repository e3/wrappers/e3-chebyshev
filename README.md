
e3-chebyshev  
======
ESS Site-specific EPICS module : chebyshev

# Introduction

This module provides a software implementation of using [Chebyshev polynomials](https://en.wikipedia.org/wiki/Chebyshev_polynomials)
for approximating a given function. This is used in particular for calibrating temperature sensors.

## Chebyshev polynomials

The `n`-th chebyshev polynomial is defined as
```
T_n(x) = cos(n * arccos(x))
```
The first few of these are given by
```
T_0(x) = 1
T_1(x) = x
T_2(x) = 2x^2 - 1
T_3(x) = 4x^3 - 3x
```
They are a collection of polynomials that are particularly well suited for approximation purposes
when approximating a function defined on the interval `-1 < x < 1`; more generally, we can scale
our input (and split it into several intervals) in order to use these polynomials effectively.

# Usage

This module adds some subroutines that can be called by an EPICS `aSub` record in order to
perform Chebyshev approximation. In order to use the module within e3, you should include the
following into a startup snippet:
```sh
require chebyshev
dbLoadRecords("$(chebyshev_DB)/aSubChebSimple.db", "IOCNAME=$(IOCNAME)")
```
This is the simplest version, with only a single interval. The interval is specified by the record
`$(IOCNAME):cheb_calc.INPB` and `$(IOCNAME):cheb_calc.INPD`, while the coefficients are specified by
`$(IOCNAME):cheb_calc.INPC` (note that C is between B and D). In general, for multiple intervals, 
you specify their endpoints by the PVS `.INPB`, `.INPD`, `.INPF`, ... and the coefficients for the
interval between those endpoints is the intermediate input field.

The input value is then `$(IOCNAME):cheb_calc.INPA` and its output is specified by
`$(IOCNAME):cheb_calc.OUTA`.

For a more complex example, look at `aSubCheb.db` included with the module.